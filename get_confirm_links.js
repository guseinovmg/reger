'use strict';

const Nightmare = require('nightmare');
const log = console.log;
//const random = require('./random.js');
const querystring = require('querystring');
const request = require('request');
const mongodb = require('mongodb');

let db;

mongodb.connect('mongodb://localhost:27017/local', (err, _db)=> {
    db = _db;
    scrap(1);
});

var mail_ids = [];

function scrap(counter) {
    return new Promise((resolve, reject)=> {
        var nightmare = Nightmare({
            show: true,
            switches: {
                'ignore-certificate-errors': true
            },
            x: 0, y: 0, width: 1200
        });
        nightmare
            .goto('https://e.mail.ru/messages/folder/1/?page=' + counter)
            .wait('#PH_logoutLink')
            .evaluate(function (counter) {
                return {
                    next: location.search === '?page=' + counter,
                    ids: arMailRuMessages.map(e=>e.id)
                };
            }, counter)
            .end()
            .then(res=> {
                mail_ids = mail_ids.concat(res.ids);
                log(res.next, res.ids.length, counter);
                if (res.next) {
                    scrap(counter + 1);
                } else {
                    log(mail_ids);
                    log(`Начинаем открывать каждое письмо`);
                    getConfirmUrl(0);
                }
            })
            .catch(err=> log(err));
    });
}

const email_selector = '#b-letter > div.b-letter.b-letter_expanded > div.b-letter__head.b-letter__head_threads > div.b-letter__head__wrapper.js-head > div.js-contacts.b-contact-container > div.b-letter__head__addrs > span.b-letter__head__addrs__value > span';

function getConfirmUrl(counter) {
    var confirm_mail_url_id = mail_ids[counter];
    var confirm_url_selector = `#style_${confirm_mail_url_id}_BODY > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td > a > table > tbody > tr > td > a`;

    const nightmare = Nightmare({
        show: true,
        switches: {
            'ignore-certificate-errors': true
        },
        x: 0, y: 0, width: 1200, height: 800
    });
    nightmare
        .goto(`https://e.mail.ru/message/${confirm_mail_url_id}/`)
        .wait(email_selector)
        .evaluate((email_selector, confirm_url_selector)=> {
            return {
                email: document.querySelector(email_selector).getAttribute('data-contact-informer-email'),
                url: document.querySelector(confirm_url_selector).href
            };
        }, email_selector, confirm_url_selector)
        .end()
        .then(res=> {
            log(counter, res.email);
            let confirm_url = res.url.indexOf('https%3A%2F%2Fpost') === -1 ? res.url : res.url.substring(res.url.indexOf('https%3A%2F%2Fpost'), res.url.indexOf('&msgid='));
            confirm_url = querystring.unescape(confirm_url);
            db.collection('users').updateOne({_id: res.email}, {
                $set: {
                    confirm_url,
                    confirm_mail_url_id
                }
            }, (err, res)=> {
                log('mongo err: ', err, res);
                getConfirmUrl(counter + 1);
            });
        })
        .catch(err=> log(err));
}




