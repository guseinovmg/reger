'use strict';

const Nightmare = require('nightmare');
const log = console.log;
const random = require('./random_user.js');
const user = random.randomUser();
var nightmare = Nightmare({
    show: false,
    switches: {
        'proxy-server': user.proxy,
        'ignore-certificate-errors': true
    }

});

nightmare.goto('https://www.pinterest.com')
    .evaluate(()=> {
        return document.getElementsByTagName('title')[0].innerText;
    })
    .end()
    .then(res=> {
        log(res);
    });