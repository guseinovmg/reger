"use strict";

const fs = require('fs');

const log = console.log;

const proxies = fs.readFileSync('./data/proxy.txt', 'utf-8').split('\n');

const male = fs.readFileSync('./data/male.txt', 'utf-8').split('\n').map(name=> name[0] + name.toLowerCase().substr(1));

const female = fs.readFileSync('./data/female.txt', 'utf-8').split('\n');

const surnames = fs.readFileSync('./data/surnames.txt', 'utf-8').split('\n');

const user_agents = fs.readFileSync('./data/user_agents.txt', 'utf-8').split('\n');

function randElem(arr) {
    return arr[parseInt((Math.random() * arr.length * 0.999))];
}

function randPassword() {
    const length = parseInt(Math.random() * 5) + 9;
    let pass = '';
    for (let i = 0; i < length; i++) {
        pass += randElem('cqwertyuiopasdfghjklzxcvbnmqwertyuioasdfghj1234567890');
    }
    return pass;
}

module.exports.randomUser = ()=> {
    let sex = Math.random() > 0.65 ? 'male' : 'female';
    let name = sex == 'male' ? randElem(male) : randElem(female);
    let surname = randElem(surnames);
    let password = randPassword();
    let user_agent = randElem(user_agents);
    let proxy = randElem(proxies);
    if (Math.random() > 0.72) {
        name = name.toLowerCase();
        surname = surname.toLowerCase()
    }
    let age = randElem([15, 16, 17, 17, 18, 18, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 23, 23, 23, 24, 24, 24, 25, 26, 27, 28, 29, 30, 34, 35, 37].map(e=>'' + e));
    let delimeter1 = randElem(['.', '_', 'z', '.', '.', 'a', 'w', '', '', '', 't', 'u', '', '', 'x', '', '', '']);
    let delimeter2 = randElem(['.', '_', 'j', '_', '', '', 'm', 'q', 'k', '', 'x', 'u', '', '', 'w', '', '', '']);

    let email_end = '' + Math.random() > 0.37 ? parseInt(Math.random() * 741 + 177) : Math.random() > 0.9 ? parseInt(Math.random() * 42 + 1981) : parseInt(Math.random() * 8000 + 1981);
    let email = '';
    if (Math.random() > 0.3) {
        email += name + delimeter1 + surname
    } else {
        if (Math.random() > 0.54) {
            email += name + delimeter1 + surname[0] + surname[1] + randElem('cqwertyuiopasdfghjklzxcvbnmqwertyuioasdfghj');
        } else {
            email += name[0] + name[1] + randElem('cqwertyuiopasdfghjklzxcvbnmqwertyuioasdfghj') + delimeter1 + surname;
        }
        email += delimeter2 + email_end;
    }
    email += '@' + randElem([
            'boomer.pw',
            'boomer.pw',
            'boomer.pw',
            'boomer.pw',
            'boomer.pw',
            'alcaloid.xyz',
            'alcaloid.xyz',
            'alcaloid.xyz',
            'galaxy-hero.xyz',
            'galaxy-hero.xyz',
            'galaxy-hero.xyz',
            'megahelper.online',
            'megahelper.online',
            'megahelper.online',
            'megahelper.online',
            'jujuka.xyz',
            'jujuka.xyz'
        ]);
    email = email.toLowerCase();

    let fullname = name + (Math.random() > 0.67 ? ' ' + surname : '');

    let interests = (()=> {
        let st = new Set();
        while (st.size !== 5) {
            st.add(parseInt(Math.random() * 14) + 1);
            var arr = [];
            st.forEach((e)=> arr.push(e));
        }
        return arr;
    })();

    let viewport = randElem([
        {x: parseInt(Math.random() * 200 + 1000), y: parseInt(Math.random() * 180 + 750)},
        {x: parseInt(Math.random() * 250 + 1000), y: parseInt(Math.random() * 200 + 750)}
    ]);

    let timestamp = Date.now();
    let currentDate = new Date();
    let day = currentDate.getDate();
    let month = currentDate.getMonth() + 1;
    let year = currentDate.getFullYear();
    let date = `${day}/${month}/${year}`;
    return {
        _id: email,
        password,
        timestamp,
        date,
        sex,
        age,
        fullname,
        user_agent,
        proxy,
        info: {
            interests,
            viewport
        }
    }
};
