'use strict';

const Nightmare = require('nightmare');
const log = console.log;
const random = require('./random_user.js');
const request = require('request');
const fs = require('fs');
const mongodb = require('mongodb');

let db;

mongodb.connect('mongodb://localhost:27017/local', (err, _db)=> {
    db = _db;
    try {
        fs.statSync(__dirname + '/last_user');
        clean_cookies()
    } catch (err) {
        creater(0);
        return;
    }
});

function createUser() {
    return new Promise((resolve, reject)=> {
        let user = random.randomUser();
        fs.writeFileSync('last_user', JSON.stringify(Object.assign({}, user, {info: null})));
        var nightmare = Nightmare({
            show: false,
            switches: {
                'proxy-server': user.proxy,
                'ignore-certificate-errors': true
            },
            x: 0,
            y: 0,
            width: user.info.viewport.x,
            height: user.info.viewport.y
        });

        let done = false;

        setTimeout(()=> {
            if (!done) {
                if (user.info.cookies) {
                    db.collection('users').insertOne(user, (err, res)=> {
                        if (err) {
                            log(err);
                        }
                        throw 'скрипт завис на 150 сек, рестартуем его';
                    });
                } else {
                    throw 'скрипт завис на 150 сек, рестартуем его';
                }
            }
        }, 150 * 1000);

        nightmare
            .useragent(user.user_agent)
            .goto('https://www.pinterest.com')
            .wait(1005)
            .cookies.clear()
            .refresh()
            .wait(1230)
            .click('#userEmail')
            .type('#userEmail', user._id)
            .wait(2200)
            .click('#userPassword')
            .type('#userPassword', user.password)
            .click('.continueButton')
            .wait(2200)
            .wait('#userFullName')
            .type('#userFullName', user.fullname)
            .wait(500)
            .type('#userAge', user.age)
            .wait(1088)
            .click(`.formInlineCheckedSet > ul:nth-child(1) > li:nth-child(${user.sex == 'male' ? 2 : 1}) > label:nth-child(1) > input:nth-child(1)`)
            .wait(1088)
            .click('.signup')
            .wait(1330)
            .wait('div.item:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)')
            .wait(1330)
            .click(`div.item:nth-child(${user.info.interests[0]}) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)`)
            .wait(457)
            .click(`div.item:nth-child(${user.info.interests[1]}) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)`)
            .wait(777)
            .click(`div.item:nth-child(${user.info.interests[2]}) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)`)
            .wait(379)
            .click(`div.item:nth-child(${user.info.interests[3]}) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)`)
            .wait(777)
            .click(`div.item:nth-child(${user.info.interests[4]}) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)`)
            .wait(544)
            .click('button.hidden > span:nth-child(1)')
            .wait(1340)
            .wait('.optionalSkip')
            .click('.optionalSkip')
            .wait(2340)
            .wait('.confirm')
            .click('.confirm')
            .wait(8000)
            .wait(`.usernameLink`)
            .wait('div.item:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > button:nth-child(1)')
            .click('div.item:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > button:nth-child(1)')
            .wait('input.name')
            .click('li.item:nth-child(1) > div:nth-child(1) > span:nth-child(1) > span:nth-child(2)')
            .wait(200)
            .click('.submitButton > button:nth-child(1) > span:nth-child(1)')
            .wait(3740)
            .cookies.get()
            .then(cookies=> {
                user.info.cookies = cookies;
                db.collection('users').insertOne(user, (err, res)=> {
                    if (err) {
                        log(err);
                    }
                });
                return nightmare.cookies.clear().refresh().end().then(()=> {
                    done = true;
                    resolve();
                });
            });
    });
}

function creater(counter) {
    createUser().then(()=> {
        log(counter);
        if (counter < 300) {
            counter++;
            setTimeout(_=> {
                creater(counter);
            }, 3000);
        }
    }).catch(err=> {
        throw err;
    });
}


function clean_cookies() {
    const user = JSON.parse(fs.readFileSync('last_user', 'utf-8'));
    const nightmare = Nightmare({
        show: true,
        switches: {
            'proxy-server': user.proxy,
            'ignore-certificate-errors': true
        },
        x: 0,
        y: 0,
        width: user.info.viewport.x,
        height: user.info.viewport.y
    });
    nightmare
        .useragent(user.user_agent)
        .goto('https://www.pinterest.com')
        .wait(1300)
        .cookies.clear()
        .refresh()
        .end()
        .then(_=> {
            creater(0);
        })
        .catch(err=> log(err));
}





