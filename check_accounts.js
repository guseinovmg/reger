const Nightmare = require('nightmare');
const log = console.log;
const request = require('request');
const mongodb = require('mongodb');

let db, users;

mongodb.connect('mongodb://localhost:27017/local', (err, _db)=> {
    db = _db;
    db.collection('users').find({
        checked: {$exists: false}
    }).toArray().then(res=> {
        users = res;
        try {
            fs.statSync(__dirname + '/last_user');
            clean_cookies()
        } catch (err) {
            checker(0);
            return;
        }
    });

});

function tryLogin(user) {
    return new Promise((resolve, reject)=> {
        var nightmare = Nightmare({
            show: true,
            switches: {
                'proxy-server': user.proxy,
                'ignore-certificate-errors': true
            },
            x: 0,
            y: 0,
            width: user.info.viewport.x,
            height: user.info.viewport.y
        });
        let done = false;
        setTimeout(()=> {
            if (!done) {
                db.collection('users').updateOne({_id: user._id}, {
                    $set: {
                        checked: false
                    }
                }, (err, res)=> {
                    throw 'скрипт завис на 50 сек, рестартуем его';
                });
            }
        }, 50 * 1000);

        nightmare
            .useragent(user.user_agent)
            .goto('https://www.pinterest.com/login/?referrer=home_page')
            .cookies.clear()
            .refresh()
            .wait(700)
            .click('li.loginUsername > input')
            .type('li.loginUsername > input', user._id)
            .wait(700)
            .click('li.loginPassword > input')
            .type('li.loginPassword > input', user.password)
            .click('body > div.App.AppBase.Module.content_only.unauth > div.appContent > div.mainContainer > div > div > div > form > div.formFooter > div > button > span')
            .wait('.usernameLink')
            .cookies.get()
            .then(cookies=> {
                return nightmare.cookies.clear().end().then(()=> {
                    db.collection('users').updateOne({_id: user._id}, {
                        $set: {
                            "info.cookies": cookies,
                            checked: true
                        }
                    }, (err, res)=> {
                        if (err) {
                            reject(err);
                        } else {
                            done = true;
                            resolve(res);
                        }
                    });
                });
            });
    });
}

function checker(counter) {
    let user = users[counter];
    log(user._id, counter);
    tryLogin(user).then(res=> {
        if (counter < users.length - 2) {
            setTimeout(_=> {
                checker(counter + 1);
            }, 2000);
        } else {
            log(`Finish!`);
        }
    }).catch(err=> {
        throw err;
    });
}


function clean_cookies() {
    const user = JSON.parse(fs.readFileSync('last_user', 'utf-8'));
    const nightmare = Nightmare({
        show: true,
        switches: {
            'proxy-server': user.proxy,
            'ignore-certificate-errors': true
        },
        x: 0,
        y: 0,
        width: user.info.viewport.x,
        height: user.info.viewport.y
    });
    nightmare
        .useragent(user.user_agent)
        .goto('https://www.pinterest.com')
        .wait(1300)
        .cookies.clear()
        .refresh()
        .end()
        .then(_=> {
            checker(0);
        })
        .catch(err=> log(err));
}