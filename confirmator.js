const Nightmare = require('nightmare');
const log = console.log;
const request = require('request');
const mongodb = require('mongodb');

let db, users;

mongodb.connect('mongodb://localhost:27017/local', (err, _db)=> {
    db = _db;
    db.collection('users').find({
        confirm_url: {$exists: true},
        mail_confirmed: {$exists: false}
    }).toArray().then(res=> {
        users = res;
        try {
            fs.statSync(__dirname + '/last_user');
            clean_cookies()
        } catch (err) {
            confirmator(0);
            return;
        }
    });

});

function confirm(user) {
    return new Promise((resolve, reject)=> {
        var nightmare = Nightmare({
            show: false,
            switches: {
                'proxy-server': user.proxy,
                'ignore-certificate-errors': true
            },
            x: 0,
            y: 0,
            width: user.info.viewport.x,
            height: user.info.viewport.y
        });

        nightmare
            .useragent(user.user_agent)
            .goto('https://www.pinterest.com')
            .cookies.set(user.info.cookies)
            .goto(user.confirm_url)
            .cookies.get()
            .then(cookies=> {
                return nightmare.cookies.clear().refresh().end().then(()=> {
                    db.collection('users').updateOne({_id: user._id}, {
                        $set: {
                            "info.cookies": cookies,
                            mail_confirmed: true
                        }
                    }, (err, res)=> {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(res);
                        }
                    });
                });
            });
    });
}

function confirmator(counter) {
    let user = users[counter];
    log(user._id, counter);
    confirm(user).then(res=> {
        if (counter < users.length - 2) {
            setTimeout(_=> {
                confirmator(counter + 1);
            }, 3000);
        } else {
            log(`Finish!`);
        }
    }).catch(err=> {
        throw err;
    });
}


function clean_cookies() {
    const user = JSON.parse(fs.readFileSync('last_user', 'utf-8'));
    const nightmare = Nightmare({
        show: true,
        switches: {
            'proxy-server': user.proxy,
            'ignore-certificate-errors': true
        },
        x: 0,
        y: 0,
        width: user.info.viewport.x,
        height: user.info.viewport.y
    });
    nightmare
        .useragent(user.user_agent)
        .goto('https://www.pinterest.com')
        .wait(1300)
        .cookies.clear()
        .refresh()
        .end()
        .then(_=> {
            confirmator(0);
        })
        .catch(err=> log(err));
}